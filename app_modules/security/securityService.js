/**
 * Created by cvwj on 04/07/14.
 */
var UUID = require('uuid-js');
var db = require('../redis')
var q = require('q')


module.exports.newToken = newToken
module.exports.authenticateToken = storeUserToken


function newToken() {
    var uuid4 = UUID.create();
    console.log(uuid4.toString());
    return uuid4
}

function storeUserToken(token, username) {
    var tokenKey = "token:" + token
    var userKey = "token:" + username
    var tenMinutesInSeconds = 60 * 1;
    var defer = q.defer()

    var oldToken
    db.get(userKey, function(err, reply) {
        oldToken = reply
    })

    var multi = db.multi()
    multi.del(oldToken)
    multi.set(tokenKey, username, "ex", tenMinutesInSeconds)
    multi.set(userKey, token, "ex", tenMinutesInSeconds)
    multi.exec(function(err, replies) {
        console.log(replies); // 102, 3
        defer.resolve()
    })

    return defer.promise
}

function getUserFromToken(token) {
    var tokenKey = "token:" + token
    var defer = q.defer()

    db.get(tokenKey, function(err, username) {
        if (err || !username) {
            console.log("Token %s does not exists", token)
            defer.resolve()
        }
        console.log("Token %s pointed to user %s", token, user)
        defer.resolve(username)
    })

}




