/**
 * Created by cvwj on 30/06/14.
 */
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var userService = require('../users').UserService
var bcrypt = require('bcryptjs')

// Passport session setup.
passport.serializeUser(function (user, done) {
    console.log("serializing " + user);
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    console.log("deserializing " + obj);
    done(null, obj);
});


passport.use('local-signin',
    new LocalStrategy(
        {
            passReqToCallback: true
        },
        function (req, username, password, done) {
            userService.get(username)
                .then(function (user) {
                    console.log("Login: Got user: " + user);
                    if (user && validPassword(user, password)) {
                        console.log("LOGGED IN AS: " + JSON.stringify(user));
                        req.session.success = 'You are successfully logged in ' + user.username + '!';
                        return done(null, user);
                    }
                    else {
                        console.log("COULD NOT LOG IN");
                        req.session.error = 'Could not log user in. Please try again.'; //inform user could not log them in
                        done(null, null)
                    }
                })
                .fail(function (err) {
                    console.log("Error in login: " + err);
                    done({msg: 'Login was unsuccessful', error: err})
                });
        }
    )
);


passport.use('local-signup',
    new LocalStrategy(
        {
            passReqToCallback: true
        },
        function (req, username, password, done) {
            userService.createUser(username, password)
                .then(function (user) {
                    if (user) {
                        console.log("REGISTERED: " + user.username);
                        req.session.success = 'You are successfully registered and logged in ' + user.username + '!';
                        done(null, user);
                    }
                    if (!user) {
                        console.log("COULD NOT REGISTER");
                        req.session.error = 'That username is already in use, please try a different one.'; //inform user could not log them in
                        done(null, null);
                    }
                })
                .fail(function (err) {
                    console.log("Failed to login: " + err)
                    done(err, null)
//                    return done({msg: 'Signup was unsuccessful', error: err}, null)
                })
        }
    )
);

function validPassword(user, password) {
    return bcrypt.compareSync(password, user.password)
}