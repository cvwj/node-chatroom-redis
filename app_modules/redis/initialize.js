/**
 * Created by cvwj on 22/06/14.
 */
var redis = require('redis');

var db
if (process.env.REDISTOGO_URL) {
    var rtg   = require("url").parse(process.env.REDISTOGO_URL);
    var db = require("redis").createClient(rtg.port, rtg.hostname);
    db.auth(rtg.auth.split(":")[1]);
    console.log(JSON.stringify(rtg))
} else {
    db = redis.createClient();
}

db.on("error", function (err) {
    console.log("Error " + err);
});


db.ping(redis.print)


module.exports = db