/**
 * Created by cvwj on 22/06/14.
 */
db = require("../redis/index")
var Q = require("q");

function getNextId(sequenceName) {
    var deferred = Q.defer();

    db.incr(sequenceName, function(err, redisId) {
        deferred.resolve(redisId)
    })

    return deferred.promise
}

var IdManager = function() {
    this.getNextId = getNextId
}
module.exports = new IdManager()