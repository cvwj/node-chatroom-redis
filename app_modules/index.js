var express    = require('express')
var app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./redis/index')


require('./routes/index')(app)


io.on('connection', function(socket){
  console.log('a user connected');
  socket.broadcast.emit('hi');

  socket.on('chat message', function(msg){
      console.log("Message incoming: " + msg)
      var persistedMessage = {'ts': new Date(), 'msg': msg};
      db.rpush("messages", JSON.stringify(persistedMessage), function(err, response) {
          socket.broadcast.emit('chat message', persistedMessage);
      })
  });

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

var port = process.env.PORT || 3000;


http.listen(port, function(){
  console.log('listening on *:' + port);
});
