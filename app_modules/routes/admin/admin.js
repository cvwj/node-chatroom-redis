/**
 * Created by cvwj on 27/06/14.
 */
module.exports = function(app) {
    app.get("/admin/redis/delete/:key", app.isAuthenticated, function(req, res) {
        db = require('../../redis')
        var key = req.params.key;
        db.del(key, function(err, response) {
            if (err) {
                res.err(err)
            } else {
                res.json({"Delete ":  key, result: response})
            }
        })
    })

}