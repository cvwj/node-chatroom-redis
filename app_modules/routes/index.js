/**
 * Created by cvwj on 22/06/14.
 */
var session = require('express-session')
var passport = require('passport')

module.exports = function(app) {

    // Configure Express
    app.use(require('morgan')(':req[content-type] -> :res[content-type]'))
    app.use(corsHeaders);
    app.use(require('express').static('public'));
    app.use(require('body-parser').json())
    app.use(session({secret: 'keyboard cat'}))
    app.use(passport.initialize());
    app.use(passport.session());

    app.isAuthenticated = ensureAuthenticated

    // My routes
    require("./history/history")(app)
    require("./admin/admin")(app)
    require("./seurity")(app)
    require('../passport/config')
    app.use(logErrors)
    app.use(clientErrorHandler)
    app.use(errorHandler)
}

function corsHeaders(req, res, next) {
    var oneof = false;
    if(req.headers.origin) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if(req.headers['access-control-request-method']) {
        res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if(req.headers['access-control-request-headers']) {
        res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if(oneof) {
        res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        res.send(200);
    }
    else {
        next();
    }
};

function ensureAuthenticated(req, res, next) {
    console.log("Testing authentication")
    if (req.isAuthenticated()) { return next(); }
    req.session.error = 'Please sign in!';
    res.json(401, {msg: "This operation requires authentication"});
}


function logErrors(err, req, res, next) {
    console.error(err.stack);
    next(err);
}

function clientErrorHandler(err, req, res, next) {
    if (req.xhr) {
        res.json(500, { error: 'Something blew up!' });
    } else {
        next(err);
    }
}

function errorHandler(err, req, res, next) {
    res.status(500);
    res.json({ error: err });
}