/**
 * Created by cvwj on 22/06/14.
 */
var historyService = require('../../history/historyService')

module.exports = function(app) {
    console.log("History routes loading...")

    app.get('/history', function(req, res){
        historyService.getHistory().then(function(messages) {
            res.json(messages)
        })
    });

    app.get('/history2', app.isAuthenticated, function(req, res){
        historyService.getHistory().then(function(messages) {
            res.json(messages)
        })
    });

}

