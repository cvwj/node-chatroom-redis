/**
 * Created by cvwj on 26/06/14.
 */

var passport = require('passport')
var securityService = require('../../security/securityService')
module.exports = function (app) {
    console.log("Login...")


    app.post('/login', function(req, res, next) {
        passport.authenticate('local-signin', function(err, user, info) {
            if (err) { return next(err); }
            if (!user) { return res.json(401, {msg:"Login failed"}); }
            req.logIn(user, function(err) {
                if (err) { return next(err); }

                var token = securityService.newToken()
                securityService.storeUserToken(user.username, token)

                return res.json({msg: "success"})
            });
        })(req, res, next);
    });
}