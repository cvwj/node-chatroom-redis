/**
 * Created by cvwj on 26/06/14.
 */

var passport = require('passport')
module.exports = function (app) {
    console.log("Signin...")

    app.post('/signup', function(req, res, next) {
            passport.authenticate('local-signup', function(err, user, info) {
                if (err) { console.log("Error signing up"); return next(err); }
                if (!user) { console.log("Error signing up: Username not available"); return res.json(400, {msg:"Signup failed"}); }
                req.logIn(user, function(err) {
                    if (err) { return next(err); }
                    return res.json({msg: "success"})
                });
            })(req, res, next);
        }
    )

}