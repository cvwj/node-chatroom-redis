/**
 * Created by cvwj on 01/07/14.
 */

module.exports = function (app) {
    require("./login")(app)
    require("./signup")(app)
}
