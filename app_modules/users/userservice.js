/**
 * Created by cvwj on 22/06/14.
 */
var bcrypt = require('bcryptjs')
idManager = require("../utils/idmanager")
User = require("./usermodel")
db = require("../redis/index")
var q = require("q");

var UserService = function () {
    this.createUser = createUser
    this.get = get
}

function createUser(username, password) {
    var defer = q.defer()

    get(username).then(function(user) {
        if (user) {
            console.log("Error creating user: ")
            defer.resolve()
        } else {
            idManager.getNextId("userIds")
                .then(function(id) {
                    var hash = bcrypt.hashSync(password, 8);
                    var user = new User(id, username, hash)
                    var stringedUser = JSON.stringify(user);
                    db.hset("users", username, stringedUser, function(err, data) {
                        console.log(user)
                        defer.resolve(user)
                    })
                }, function(err) {
                    console.log("Error creating user: "+ err)
                    defer.reject(err)
                })

        }
    })

    return defer.promise

}

function get(username) {
    var defer = q.defer()
    db.hget("users", username, function(err, user) {
        if (err) {
            console.log("Error getting user: " + err)
            defer.reject(err)
        }
        else if (user)
        {
            console.log("Got user: " + user)
            defer.resolve(JSON.parse(user))
        }
        else {
            console.log("No user with username: " + username)
            defer.resolve(null)
        }
    })
    return defer.promise
}

module.exports = new UserService()