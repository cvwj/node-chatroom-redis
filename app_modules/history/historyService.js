/**
 * Created by cvwj on 27/06/14.
 */
db = require("../redis")
var q = require("q");

var HistoryService = function () {
    this.getHistory = getHistory
}

function getHistory() {
    var defer = q.defer()

    db.lrange("messages", 0, -1, function(err, messages) {
        defer.resolve(messages)
        console.log("Historical messages: " + messages)
    })
    return defer.promise

}


module.exports = new HistoryService()